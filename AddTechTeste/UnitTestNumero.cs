﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AddTechLib;

namespace AddTechTeste
{
    [TestClass]
    public class UnitTestNumero
    {
        [TestMethod]
        public void SorteFelicidade()
        {
            NumeroSortudo mNumeroSortudo = new NumeroSortudo();
            NumeroFeliz mNumeroFeliz = new NumeroFeliz();

            //1) 7 – Número Sortudo e Feliz.

            Assert.AreEqual(mNumeroSortudo.VerificaNumero(7, 100), true);
            Assert.AreEqual(mNumeroFeliz.VerificaNumero(7, 100), true);

            //2) 21 – Número Sortudo e Não-Feliz.

            Assert.AreEqual(mNumeroSortudo.VerificaNumero(21, 100), true);
            Assert.AreEqual(mNumeroFeliz.VerificaNumero(21, 100), false);

            //3) 28 – Número Não-Sortudo e Feliz.

            Assert.AreEqual(mNumeroSortudo.VerificaNumero(28, 100), false);
            Assert.AreEqual(mNumeroFeliz.VerificaNumero(28, 100), true);

            //4) 142 – Número Não-Sortudo e Não-Feliz

            Assert.AreEqual(mNumeroSortudo.VerificaNumero(142, 100), false);
            Assert.AreEqual(mNumeroFeliz.VerificaNumero(142, 100), false);

            //5) 37 – Número Sortudo e Não-Feliz

            Assert.AreEqual(mNumeroSortudo.VerificaNumero(37, 100), true);
            Assert.AreEqual(mNumeroFeliz.VerificaNumero(37, 100), false);

            //6) 100 – Número Não-Sortudo e Feliz.

            Assert.AreEqual(mNumeroSortudo.VerificaNumero(100, 100), false);
            Assert.AreEqual(mNumeroFeliz.VerificaNumero(100, 100), true);

        }
    }
}
