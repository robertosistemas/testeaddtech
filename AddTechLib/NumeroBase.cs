﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddTechLib
{
    /// <summary>
    /// Classe base como métodos compartinhado entre as classes especializadas
    /// </summary>
    public class NumeroBase
    {
        /// <summary>
        /// Verifica se os parâmetros passados são válidos
        /// </summary>
        /// <param name="numero">Número a verificar</param>
        /// <param name="interacoes">Quantidade de Interações</param>
        public static void ValidaValores(int numero, int interacoes)
        {
            if (numero < 0)
            {
                throw new ArgumentException("Não foi fornecido um número válido!", "Numero");
            }

            if (interacoes < 0)
            {
                throw new ArgumentException("Não foi fornecido um número de interações valido!", "Interacoes");
            }

            if (interacoes > 100)
            {
                throw new ArgumentException("Número de interações não pode ser maior que 100!", "Interacoes");
            }
        }

        /// <summary>
        /// Separa cada número de um valor e os devolvem como uma lista
        /// </summary>
        /// <param name="valor">valor a separar os dígitos</param>
        /// <returns>Lista de dígitos</returns>
        public static List<int> SepararNumeros(int valor)
        {

            List<int> valores = new List<int>();

            while (valor != 0)
            {
                valores.Add(valor % 10);
                valor = valor / 10;
            }
            return valores;
        }

        /// <summary>
        /// Soma o quadrado dos itens da lista
        /// </summary>
        /// <param name="lista">Lista de números a somar</param>
        /// <returns>Soma dos números elevado ao quadrado</returns>
        public static int SomarQuadrado(List<int> lista)
        {

            int soma = 0;

            foreach (int item in lista)
            {
                soma += item * item;
            }
            return soma;
        }

        /// <summary>
        /// Cria uma lista de Números até o limite fornecido
        /// </summary>
        /// <param name="valor">Valor númerico para criar uma lista</param>
        /// <returns>Cria lista começando em 1 e incrementando até o valor informado</returns>
        public static List<int> CriarLista(int valor)
        {

            List<int> lista = new List<int>();

            for (int x = 1; x <= valor; x++)
            {
                lista.Add(x);
            }
            return lista;
        }

        /// <summary>
        /// Remove elementos em que a posição na lista é múltiplo do valor fornecido 
        /// </summary>
        /// <param name="numero">Número de referência para remover os itens de posição múltipla</param>
        /// <param name="lista">Lista de completa de números</param>
        /// <returns>Lista de números sem os itens removidos</returns>
        public static List<int> RemoverMultiplos(int numero, List<int> lista)
        {

            List<int> novaLista = new List<int>();

            int pos = 1;

            foreach (int item in lista)
            {
                if ((pos % numero) != 0)
                {
                    novaLista.Add(item);
                }
                pos++;
            }
            return novaLista;
        }

    }
}
