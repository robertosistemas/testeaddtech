﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddTechLib
{
    /// <summary>
    /// Classe  para verificar se um número é sortudo. Herda de NumeroBase e implementa a interface INumero
    /// </summary>
    public class NumeroSortudo : NumeroBase, INumero
    {

        /// <summary>
        /// Verifica se o número é Sortudo
        /// </summary>
        /// <param name="numero">Número a verificar</param>
        /// <param name="interacoes">Quantidades de interações</param>
        /// <returns>true se é sortudo ou false se não for sortudo</returns>
        public bool VerificaNumero(int numero, int interacoes)
        {
            ValidaValores(numero, interacoes);

            List<int> listaNumeros = CriarLista(numero);

            int i = 1;

            listaNumeros = RemoverMultiplos(2, listaNumeros); //Remove todos os números pares

            while ((i < listaNumeros.Count - 1) && (i <= interacoes - 1))
            {
                listaNumeros = RemoverMultiplos(listaNumeros[i], listaNumeros);
                i++;
            }
            return listaNumeros.Contains<int>(numero);
        }

        /// <summary>
        /// Gera uma lista de números sortudos de 0 até o número e interações fornecidos
        /// </summary>
        /// <param name="numero"></param>
        /// <param name="interacoes"></param>
        /// <returns></returns>
        public List<int> GerarListaNumerosSortudos(int numero, int interacoes)
        {

            List<int> lista = new List<int>();

            for (int i = 0; i <= numero; i++)
            {

                if (VerificaNumero(i, interacoes))
                {
                    lista.Add(i);
                }
            }
            return lista;
        }
    }
}
