﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddTechLib
{
    interface INumero
    {
        /// <summary>
        /// Método a ser implementado para verificação de número
        /// </summary>
        /// <param name="numero">Número a verificar</param>
        /// <param name="interacoes">Quantidade de Interações</param>
        /// <returns>true ou false</returns>
        bool VerificaNumero(int numero, int interacoes);
    }
}
