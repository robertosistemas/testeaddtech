﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddTechLib
{
    /// <summary>
    /// Classe  para verificar se um número é feliz. Herda de NumeroBase e implementa a interface INumero
    /// </summary>
    public class NumeroFeliz : NumeroBase, INumero
    {
        /// <summary>
        /// Verifica se o número é Feliz
        /// </summary>
        /// <param name="numero">Número a verificar</param>
        /// <param name="interacoes">Quantidades de interações</param>
        /// <returns>true se é feliz ou false se não for feliz</returns>
        public bool VerificaNumero(int numero, int interacoes)
        {

            ValidaValores(numero, interacoes);

            bool Feliz = false;

            List<int> listaNumeros = new List<int>();

            listaNumeros = SepararNumeros(numero);

            for (int i = 0; i < interacoes && !Feliz; i++)
            {
                int soma = SomarQuadrado(listaNumeros);

                if (soma == 1)
                {
                    Feliz = true;
                }
                else
                {
                    listaNumeros = SepararNumeros(soma);
                }
            }
            return Feliz;
        }

        /// <summary>
        /// Gera uma lista de números felizes de 0 até o número e interações fornecidos
        /// </summary>
        /// <param name="numero"></param>
        /// <param name="interacoes"></param>
        /// <returns></returns>
        public List<int> GerarListaNumerosFelizes(int numero, int interacoes)
        {

            List<int> lista = new List<int>();

            for (int i = 0; i <= numero; i++)
            {
                if (VerificaNumero(i, interacoes))
                {
                    lista.Add(i);
                }
            }
            return lista;
        }

    }
}
