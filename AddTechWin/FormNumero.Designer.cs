﻿namespace AddTechWin
{
    partial class FormNumero
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.butGerarListaNumerosFelizes = new System.Windows.Forms.Button();
            this.lsbNumeros = new System.Windows.Forms.ListBox();
            this.butTestarFeliz = new System.Windows.Forms.Button();
            this.butTestarSortudo = new System.Windows.Forms.Button();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.txtInteracoes = new System.Windows.Forms.TextBox();
            this.butGerarListaNumerosSortudos = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grbNumero = new System.Windows.Forms.GroupBox();
            this.grbInteracoes = new System.Windows.Forms.GroupBox();
            this.grbNumeros = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.butSair = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.grbNumero.SuspendLayout();
            this.grbInteracoes.SuspendLayout();
            this.grbNumeros.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // butGerarListaNumerosFelizes
            // 
            this.butGerarListaNumerosFelizes.Location = new System.Drawing.Point(209, 101);
            this.butGerarListaNumerosFelizes.Name = "butGerarListaNumerosFelizes";
            this.butGerarListaNumerosFelizes.Size = new System.Drawing.Size(176, 23);
            this.butGerarListaNumerosFelizes.TabIndex = 0;
            this.butGerarListaNumerosFelizes.Text = "Gerar Lista de Números Felizes";
            this.butGerarListaNumerosFelizes.UseVisualStyleBackColor = true;
            this.butGerarListaNumerosFelizes.Click += new System.EventHandler(this.butGerarListaNumerosFelizes_Click);
            // 
            // lsbNumeros
            // 
            this.lsbNumeros.FormattingEnabled = true;
            this.lsbNumeros.Location = new System.Drawing.Point(17, 19);
            this.lsbNumeros.MultiColumn = true;
            this.lsbNumeros.Name = "lsbNumeros";
            this.lsbNumeros.Size = new System.Drawing.Size(338, 173);
            this.lsbNumeros.TabIndex = 1;
            // 
            // butTestarFeliz
            // 
            this.butTestarFeliz.Location = new System.Drawing.Point(209, 12);
            this.butTestarFeliz.Name = "butTestarFeliz";
            this.butTestarFeliz.Size = new System.Drawing.Size(176, 23);
            this.butTestarFeliz.TabIndex = 3;
            this.butTestarFeliz.Text = "Testar se é Feliz";
            this.butTestarFeliz.UseVisualStyleBackColor = true;
            this.butTestarFeliz.Click += new System.EventHandler(this.butTestarFeliz_Click);
            // 
            // butTestarSortudo
            // 
            this.butTestarSortudo.Location = new System.Drawing.Point(209, 41);
            this.butTestarSortudo.Name = "butTestarSortudo";
            this.butTestarSortudo.Size = new System.Drawing.Size(176, 23);
            this.butTestarSortudo.TabIndex = 4;
            this.butTestarSortudo.Text = "Testar se é Sortudo";
            this.butTestarSortudo.UseVisualStyleBackColor = true;
            this.butTestarSortudo.Click += new System.EventHandler(this.butTestarSortudo_Click);
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(17, 19);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(140, 20);
            this.txtNumero.TabIndex = 5;
            this.txtNumero.Text = "100";
            // 
            // txtInteracoes
            // 
            this.txtInteracoes.Location = new System.Drawing.Point(17, 19);
            this.txtInteracoes.Name = "txtInteracoes";
            this.txtInteracoes.Size = new System.Drawing.Size(140, 20);
            this.txtInteracoes.TabIndex = 7;
            this.txtInteracoes.Text = "100";
            // 
            // butGerarListaNumerosSortudos
            // 
            this.butGerarListaNumerosSortudos.Location = new System.Drawing.Point(209, 72);
            this.butGerarListaNumerosSortudos.Name = "butGerarListaNumerosSortudos";
            this.butGerarListaNumerosSortudos.Size = new System.Drawing.Size(176, 23);
            this.butGerarListaNumerosSortudos.TabIndex = 8;
            this.butGerarListaNumerosSortudos.Text = "Gerar Lista de Números Sortudos";
            this.butGerarListaNumerosSortudos.UseVisualStyleBackColor = true;
            this.butGerarListaNumerosSortudos.Click += new System.EventHandler(this.butGerarListaNumerosSortudos_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butTestarFeliz);
            this.panel1.Controls.Add(this.butGerarListaNumerosSortudos);
            this.panel1.Controls.Add(this.butTestarSortudo);
            this.panel1.Controls.Add(this.butGerarListaNumerosFelizes);
            this.panel1.Controls.Add(this.grbNumero);
            this.panel1.Controls.Add(this.grbInteracoes);
            this.panel1.Controls.Add(this.grbNumeros);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(399, 337);
            this.panel1.TabIndex = 9;
            // 
            // grbNumero
            // 
            this.grbNumero.Controls.Add(this.txtNumero);
            this.grbNumero.Location = new System.Drawing.Point(12, 69);
            this.grbNumero.Name = "grbNumero";
            this.grbNumero.Size = new System.Drawing.Size(176, 51);
            this.grbNumero.TabIndex = 10;
            this.grbNumero.TabStop = false;
            this.grbNumero.Text = "Número";
            // 
            // grbInteracoes
            // 
            this.grbInteracoes.Controls.Add(this.txtInteracoes);
            this.grbInteracoes.Location = new System.Drawing.Point(12, 12);
            this.grbInteracoes.Name = "grbInteracoes";
            this.grbInteracoes.Size = new System.Drawing.Size(176, 51);
            this.grbInteracoes.TabIndex = 9;
            this.grbInteracoes.TabStop = false;
            this.grbInteracoes.Text = "Interações";
            // 
            // grbNumeros
            // 
            this.grbNumeros.Controls.Add(this.lsbNumeros);
            this.grbNumeros.Location = new System.Drawing.Point(12, 126);
            this.grbNumeros.Name = "grbNumeros";
            this.grbNumeros.Size = new System.Drawing.Size(373, 208);
            this.grbNumeros.TabIndex = 8;
            this.grbNumeros.TabStop = false;
            this.grbNumeros.Text = "Lista de Números";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.butSair);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 337);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(399, 37);
            this.panel2.TabIndex = 10;
            // 
            // butSair
            // 
            this.butSair.Location = new System.Drawing.Point(310, 6);
            this.butSair.Name = "butSair";
            this.butSair.Size = new System.Drawing.Size(75, 23);
            this.butSair.TabIndex = 0;
            this.butSair.Text = "Sair";
            this.butSair.UseVisualStyleBackColor = true;
            this.butSair.Click += new System.EventHandler(this.butSair_Click);
            // 
            // FormNumero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 374);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "FormNumero";
            this.Text = "Números Sortudos e Felizes";
            this.panel1.ResumeLayout(false);
            this.grbNumero.ResumeLayout(false);
            this.grbNumero.PerformLayout();
            this.grbInteracoes.ResumeLayout(false);
            this.grbInteracoes.PerformLayout();
            this.grbNumeros.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button butGerarListaNumerosFelizes;
        private System.Windows.Forms.ListBox lsbNumeros;
        private System.Windows.Forms.Button butTestarFeliz;
        private System.Windows.Forms.Button butTestarSortudo;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.TextBox txtInteracoes;
        private System.Windows.Forms.Button butGerarListaNumerosSortudos;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox grbNumeros;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox grbNumero;
        private System.Windows.Forms.GroupBox grbInteracoes;
        private System.Windows.Forms.Button butSair;
    }
}

