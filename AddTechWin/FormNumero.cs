﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AddTechLib;

namespace AddTechWin
{
    public partial class FormNumero : Form
    {
        public FormNumero()
        {
            InitializeComponent();
        }

        private void butGerarListaNumerosFelizes_Click(object sender, EventArgs e)
        {
            try
            {
                butGerarListaNumerosFelizes.Enabled = false;

                int numero = Convert.ToInt32(txtNumero.Text);
                int interacoes = Convert.ToInt32(txtInteracoes.Text);

                NumeroFeliz mNumeroFeliz = new NumeroFeliz();

                lsbNumeros.BeginUpdate();
                lsbNumeros.DataSource = null;
                lsbNumeros.Items.Clear();
                lsbNumeros.DataSource = mNumeroFeliz.GerarListaNumerosFelizes(numero, interacoes);
                lsbNumeros.EndUpdate();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                butGerarListaNumerosFelizes.Enabled = true;
            }
        }

        private void butGerarListaNumerosSortudos_Click(object sender, EventArgs e)
        {
            try
            {
                butGerarListaNumerosSortudos.Enabled = false;

                int numero = Convert.ToInt32(txtNumero.Text);
                int interacoes = Convert.ToInt32(txtInteracoes.Text);

                NumeroSortudo mNumeroSortudo = new NumeroSortudo();

                lsbNumeros.BeginUpdate();
                lsbNumeros.DataSource = null;
                lsbNumeros.Items.Clear();
                lsbNumeros.DataSource = mNumeroSortudo.GerarListaNumerosSortudos(numero, interacoes);
                lsbNumeros.EndUpdate();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                butGerarListaNumerosSortudos.Enabled = true;
            }
        }

        private void butTestarFeliz_Click(object sender, EventArgs e)
        {
            try
            {
                int numero = Convert.ToInt32(txtNumero.Text);
                int interacoes = Convert.ToInt32(txtInteracoes.Text);

                NumeroFeliz mNumeroFeliz = new NumeroFeliz();

                if (mNumeroFeliz.VerificaNumero(numero, interacoes))
                {
                    MessageBox.Show(string.Format("O Número {0} é feliz!", numero));
                }
                else
                {
                    MessageBox.Show(string.Format("O Número {0} não é feliz!", numero));
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void butTestarSortudo_Click(object sender, EventArgs e)
        {
            try
            {
                int numero = Convert.ToInt32(txtNumero.Text);
                int interacoes = Convert.ToInt32(txtInteracoes.Text);

                NumeroSortudo mNumeroSortudo = new NumeroSortudo();

                if (mNumeroSortudo.VerificaNumero(numero, interacoes))
                {
                    MessageBox.Show(string.Format("O Número {0} é sortudo!", numero));
                }
                else
                {
                    MessageBox.Show(string.Format("O Número {0} não é sortudo!", numero));
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void butSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
